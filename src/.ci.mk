#
#-------------------------------------------------------------------------------
 PLT=Linux_x8664
#-------------------------------------------------------------------------------

 PLTOBJECTS = readTSC.o

 CXX=mpicxx
 LD=$(CXX)

 PLTFLAGS += -DIA32 -D_LARGEFILE_SOURCE \
             -D_FILE_OFFSET_BITS=64 -DUSE_MPI -DSCALAPACK -DADD_ \
             -DAPP_NO_THREADS -DXML_USE_NO_THREADS -DUSE_XERCES \
             -DXERCESC_3 -DMPICH_IGNORE_CXX_SEEK

# FFT must be FFTW2, FFTW3, ESSL or NOLIB
 FFT=FFTW3

ifeq ($(FFT),FFTW2)
 PLTFLAGS += -DUSE_FFTW2
#PLTFLAGS += -DFFTWMEASURE
#FFTWDIR=$(HOME)/software/fftw/Linux_x8664/fftw-2.1.5/fftw
#FFTWINCLUDEDIR=$(FFTWDIR)
#FFTWLIBDIR=$(FFTWDIR)/.libs
#INCLUDE += -I$(FFTWINCLUDEDIR)
#LIBPATH += -L$(FFTWLIBDIR)
 LIBS += -lfftw
endif

ifeq ($(FFT),FFTW3)
 PLTFLAGS += -DUSE_FFTW3
#PLTFLAGS += -DFFTWMEASURE
#PLTFLAGS += -DFFTW_TRANSPOSE
 PLTFLAGS += -DFFTW3_2D
 FFTWDIR=/tmp/fftw3
 FFTWINCLUDEDIR=$(FFTWDIR)/include
 FFTWLIBDIR=$(FFTWDIR)/lib
 INCLUDE += -I$(FFTWINCLUDEDIR)
 LIBPATH += -L$(FFTWLIBDIR)
 LIBS += -lfftw3
endif

ifeq ($(FFT),ESSL)
$(error ESSL library not available)
endif

ifeq ($(FFT),NOLIB)
 PLTFLAGS += -DFFT_NOLIB
endif

INCLUDE += -I/tmp/xerces/include
LIBS += -L/tmp/xerces/lib

LIBS += -L/usr/local/lib

 CXXFLAGS= -g -O3 -Wunused -D$(PLT) $(INCLUDE) $(PLTFLAGS) $(DFLAGS)
 LIBS += -lpthread -lxerces-c -lscalapack -llapack -lblas -lm -lgfortran
 LDFLAGS = $(LIBPATH) $(LIBS)

#-------------------------------------------------------------------------------
